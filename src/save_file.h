#pragma once
#include <iostream>
#include <fstream>
#include <climits>

// This holds the statistics for the machines
// stated here already because otherwise the save function won't compile
struct machine{
    int empty = 0;
    int full = 0;
    int most = INT_MAX;
    int mostCount = 0;
};

void fileSave(machine &M1, machine &M2, machine &M3);