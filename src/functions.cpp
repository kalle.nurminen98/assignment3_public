#include "functions.h"

// Counts the ones on a single line
int onesCount(std::vector<std::string> testLine){
    int onecount = 0;
    try{
        int vSize = testLine.size();
        int n = std::stoi(testLine[2]);
        if(vSize != n + 3){// Avoiding segmentation faults if vector is not expected length
            std::cout<<"segmentation fault avoided, skipping line\n";
            return -1;

        }
        if(n == 0){
            return 0;
        }
        for(int i = 3; i < 3 + n; i++){
            if(testLine[i] == "1"){
                onecount++;
            }
        }

        return onecount;
    }
    catch(...){// Happens at least when N doesn't translate into integer
        std::cout<<"error caught onesCount, skipping line\n";
        return -1;
    }
}
// Checks if the line is full of 1
bool isFull(std::vector<std::string> testLine){
    int onecount = 0;
    int n = std::stoi(testLine[2]);
    
    for(int i = 3; i < 3 + n; i++){
        if(testLine[i] == "1"){
            onecount++;
        }
    }
    if(onecount == n){
        return true; //machine is full
    }
    return false; //machine is empty

    return onecount;
}

//checks if the line is full of 0
bool isEmpty(std::vector<std::string> testLine){
    int onecount = 0;
    int n = std::stoi(testLine[2]);
    
    for(int i = 3; i < 3 + n; i++){
        if(testLine[i] == "1"){
            onecount++;
        }
    }

    if(onecount == 0){
        return true; //machine is empty
    }
    return false; // machine is not empty
}


