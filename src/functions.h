#pragma once
#include <string>
#include <vector>
#include <iostream>

int onesCount(std::vector<std::string> testLine);
bool isEmpty(std::vector<std::string> testLine);
bool isFull(std::vector<std::string> testLine);

