// Header file which contains configurations for this function
#include "read_file.h"

std::vector<std::vector<std::string>> read_file()
{
    // Configurating vector inside vector and string
    std::vector<std::vector<std::string>> data_read;
    std::string line;
    // Opening file
    std::ifstream file("../FleetState.txt");
    // If file is open
	if(file.is_open())
    {
        // While loop to get information from file
        while(getline(file, line))
		{
            // Configurating istringstream object and new vector and new string
            std::istringstream ss(line);
            std::vector<std::string> one_line;
            std::string value;
            // While loop for vector
            while(ss.good())
            {
                // Gets new value after comma and separates them
                getline(ss,value, ',');
                // Adds to it vector
                one_line.push_back(value);
            }
            // Adds vector to vector inside vector parameter
            data_read.push_back(one_line);
            /*
            data_read.push_back(std::vector<std::string>());
            std::stringstream split(line);
            std::string value;

            while(split >> value)
            {
                data_read.back().push_back(value);
            }
            */
		}
        // Closing file
        file.close();
	}
    // If file not found
	else
    {
	    std::cout << "File read error!" <<std::endl;
	}
    /*
    for(int i = 0; i < data_read.size();i++)
    {
        for(int j=0; j < data_read[i].size(); j++)
        {
            std::cout << data_read[i][j] << std::endl;
        }
    }
    */
    // Returns vector inside vector
	return data_read;
}