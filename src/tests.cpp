// tests.cpp
#include <string>
#include <vector>
#include "functions.cpp"
#include <gtest/gtest.h>

  // Some vectors to be used as test inputs
  std::vector<std::string> line1 = {"1","M1","6","0","0","1","0","1","0"};
  std::vector<std::string> line2 = {"1","M1","6","0","1","0","0","1","0"};
  std::vector<std::string> line3 = {"2","M1","6","0","0","0","0","0","0"};
  std::vector<std::string> line4 = {"3","M1","6","1","1","1","1","1","1"};
  std::vector<std::string> line5 = {"5","M2","6","1","0","1","1","1","1"};
  std::vector<std::string> line6 = {"8","M3","7","1","0","1","0","1","0","0"};
  std::vector<std::string> line7 = {"3","M1","8","1","1","1","1","1","1","0","0"};
  std::vector<std::string> line8 = {"5","M2","9","1","0","1","1","1","1","1","1","1"};
  std::vector<std::string> line9 = {"8","M3","10","1","0","1","0","1","0","1","0","1","0"};


//test if amount of ones in a function is correctly counted
TEST(funcsTest, onesCount)
{
  ASSERT_EQ(2, onesCount(line1));
  ASSERT_EQ(2, onesCount(line2));
  ASSERT_EQ(0, onesCount(line3));
  ASSERT_EQ(6, onesCount(line4));
  ASSERT_EQ(5, onesCount(line5));
  ASSERT_EQ(3, onesCount(line6));
  ASSERT_EQ(6, onesCount(line7));
  ASSERT_EQ(8, onesCount(line8));
  ASSERT_EQ(5, onesCount(line9));

}
// Test if full vector is properly detected
TEST(funcsTest, isFull)
{
  ASSERT_EQ(true, isFull(line4));
  ASSERT_EQ(false, isFull(line5));
  ASSERT_EQ(false, isFull(line3));

}
// Test if empty vector is properly detected
TEST(funcsTest, isEmpty)
{
  ASSERT_EQ(true, isEmpty(line3));
  ASSERT_EQ(false, isEmpty(line1));
  ASSERT_EQ(false, isEmpty(line4));

}



int main(int argc, char **argv)
{
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
