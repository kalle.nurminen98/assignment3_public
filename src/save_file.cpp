#include "save_file.h"

// Statistics are saved here
void fileSave(machine &m1, machine &m2, machine &m3){
    std::ofstream dataFile;

    std::string filePath = "../Statistics.txt";

    dataFile.open(filePath);

    dataFile <<"EMPTY: M1="<<m1.empty<<"; M2="<<m2.empty<<"; M3="<<m2.empty <<";\n\n";

    dataFile <<"FULL: M1="<<m1.full<<"; M2="<<m2.full<<"; M3="<<m3.full <<";\n\n";

    dataFile <<"MOST FILLED: M1="<<m1.most<<","<<m1.mostCount
    <<"; M2="<<m2.most<<","<<m2.mostCount<<"; M3="<<m3.most<<","<<m3.mostCount <<";\n";

    dataFile.close();


}